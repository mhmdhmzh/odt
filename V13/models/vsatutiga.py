# -*- coding: utf-8 -*-
# Copyright YEAR(S), AUTHOR(S)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from odoo import fields, models, api, _
from datetime import date, datetime, timedelta


class VSatuTiga(models.Model):
    _inherit = "sale.order"
    # case 1
    warehouse_id = fields.Many2one("stock.warehouse", string="Warehouse")
#     case 2
    is_all_warehouse = fields.Boolean("All Warehouse")

