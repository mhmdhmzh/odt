# -*- coding: utf-8 -*-
# Copyright YEAR(S), AUTHOR(S)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from odoo import fields, models, api, _
from datetime import date, datetime, timedelta


class VSatuSatuLine(models.Model):
    _inherit = "purchase.order"

    @api.depends('down_payment')
    def compute_total_dp(self):
        self.total = self.amount_total
        if self.down_payment:
            self.total = self.amount_total - self.down_payment



    total = fields.Float(string="Total", compute="compute_total_dp")
    down_payment = fields.Float(string="DP")
