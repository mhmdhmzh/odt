# -*- coding: utf-8 -*-
# Copyright YEAR(S), AUTHOR(S)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from odoo import fields, models, api, _
from datetime import date, datetime, timedelta


class VSatuEmpat(models.Model):
    _inherit = "sale.order"

    # @api.depends('order_line.price_unit')
    # def compute_total_css(self):
    #     for x in self.order_line:
    #         if x.type == "consu":
    #             self.total_consumable = sum(x.price_unit)
    #     for i in self.order_line:
    #         if i.type == "service":
    #             self.total_service = sum(i.price_unit)
    #     for b in self.order_line:
    #         if b.type == "product":
    #             self.total_storable = sum(b.price_unit)
    #     print(self.total_storable)
    #     print(self.total_consumable)
    #     print(self.total_service)


    total_consumable = fields.Float(string="Total Consumable", compute="compute_total_css")
    total_service = fields.Float(string="Total Service", compute="compute_total_css")
    total_storable = fields.Float(string="Total Storable", compute="compute_total_css")

    is_consumable = fields.Boolean("Consumable")
    is_service = fields.Boolean("Service")
    is_product = fields.Boolean("Storable")

    @api.onchange('order_line.product_id')
    def change_bool_dong(self):
        for x in self.order_line:
            pass

class VSatuEmpatLine(models.Model):
    _inherit = "sale.order.line"

    type = fields.Selection([
		('consu','Consumable'),
		('service','Service'),
		('product','Storable Product')
		],default='', string='Type', related="product_id.type")

