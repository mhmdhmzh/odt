# -*- coding: utf-8 -*-
# Copyright YEAR(S), AUTHOR(S)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from odoo import fields, models, api, _
from datetime import date, datetime, timedelta


class VSatuDua(models.Model):
    _inherit = "purchase.order"
    # case 1
    supplier_email = fields.Char(string="Supplier Email", related="partner_id.email")

# case 2
class VSatuDuaLine(models.Model):
    _inherit = "purchase.order.line"

    @api.depends('price_unit')
    def compute_difference_price(self):
        if self.price_unit:
            self.difference_price = self.price_unit - self.product_id.standard_price

    difference_price = fields.Float(string="Difference Price", compute="compute_difference_price")

