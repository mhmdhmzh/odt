from openerp import models, fields, api, _


class purchase_order(models.Model):
    _inherit = "purchase.order"

    @api.one
    @api.depends('date_planned', 'date_order')
    def get_time_of_delivery(self):
        if not self.date_planned:
            self.date_planned = fields.Date.today()
        mpd = fields.Date.from_string(self.date_planned)
        do = fields.Date.from_string(self.date_order)
        tod = mpd - do
        self.time_of_delivery = tod.days

    time_of_delivery = fields.Float('Time of Delivery (days)',
                                    compute='get_time_of_delivery')
