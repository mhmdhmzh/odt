# -*- coding: utf-8 -*-
{
    'name': "V1.5",

    'summary': """
    custom
""",

    'description': """
        custom
        
    """,

    'author': "M. Hamzah",
    'website': "https://www.twitter.com/mhmdhmzh",
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ["stock"],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        # 'security/views.xml',
        'views/views.xml',
    ],
}
