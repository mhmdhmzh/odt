# -*- coding: utf-8 -*-
# Copyright YEAR(S), AUTHOR(S)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from odoo import fields, models, api, _
from datetime import date, datetime, timedelta

class VSatuLima(models.Model):
    _inherit = "product.template"

    transaction_uom_id = fields.Many2one(
        'uom.uom', 'Transaction UoM',)
    second_uom_id = fields.Many2one(
        'uom.uom', '2nd UoM', )

    lower_limit = fields.Integer(string="Lower Limit (days)")
    upper_limit = fields.Integer(string="Upper Limit (days)")

#     dimension
    @api.depends('length', 'width', 'height')
    def itung_volume(self):
        self.volume = self.length * self.width * self.height

    length = fields.Float("Length")
    width = fields.Float("Width")
    height = fields.Float("Height")
    volume = fields.Float("Volume", compute="itung_volume")

